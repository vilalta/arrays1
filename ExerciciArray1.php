<?php
//funcio que donat un numero calcula el seu factorial fent una crida recursiva
    function factorial($num) { 
        if ($num < 2) { 
            return 1; 
        } else { 
            return ($num * factorial($num-1)); 
        } 
    }
//funcio que compleix l'enunciat de l'exercici
    function FactorialArray($array)
    {   
        $resultat = array();      
        if(is_array($array))
        {
            foreach($array as $value){
                if(!is_int($value)){return false;}
                $resultat[] = factorial($value);
            }
        }else{
            return false;
        }
        return $resultat;
    } 
//funcio que escriu, mantinguent els salts de linia, el text donat per parametres 
    function fes_pre($txt)
    {
        echo '<pre>';
        if(is_array($txt)){
          print_r($txt);
        }else{
          echo 'false';
        }        
        echo '</pre>';
    }
//joc de tres proves
    $prova1 = array(2,3,4,5,6);
    fes_pre(FactorialArray($prova1));
    $prova2 = array(2,3,true,5,6);
    fes_pre(FactorialArray($prova2));
    $prova3 = "prova";
    fes_pre(FactorialArray($prova3));
?>