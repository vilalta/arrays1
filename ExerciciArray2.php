<?php
//funcio que genera una matriu quadrada donant la mida del costat
    function generaMatriu($costat){
      $matriu = array();
      for ($i = 0; $i < $costat; $i++){
        $matriu[$i] = array();
        for($j = 0;$j < $costat;$j++){
          if($i == $j){
            $aux = '*';
          }elseif($i < $j){
            $aux = $i+$j;
          }else{
            $aux = rand(11,19);
          }
          $matriu[$i][$j] = $aux;
        }
      }
      return $matriu;
    }
// funcio que mostra la matriu donada
  function mostraMatriu($matriu){
    echo '<table>';
    foreach($matriu as $key => $value){
      echo '<tr>';
      foreach ($matriu[$key] as $valor){
        echo '<td>';
        echo $valor;
        echo '</td>';
      }
      echo '</tr>';
    }
    echo '</table>';
  }
//funcio que intercanvia files per columnes d'una matriu quadrada
function giraMatriu($matriu){
  $resultat = $matriu;
  foreach($matriu as $key => $value){
      foreach ($matriu[$key] as $clave => $valor){
        $resultat[$clave][$key] = $matriu[$key][$clave];
      }
    }
  return $resultat;
}
$exemple = generaMatriu(4);
mostraMatriu($exemple);
mostraMatriu(giraMatriu($exemple));//
?>